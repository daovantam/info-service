package com.mor.infoservice.exception.custom;

import com.mor.infoservice.constant.ResponseStatusCodeEnum;
import lombok.Getter;

@Getter
public class NoDataHasBeenChangeException extends RuntimeException {

  private final String code;

  public NoDataHasBeenChangeException(ResponseStatusCodeEnum massage) {
    super(massage.getMessage());
    this.code = massage.getCode();
  }
}
