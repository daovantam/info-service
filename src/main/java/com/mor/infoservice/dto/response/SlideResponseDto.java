package com.mor.infoservice.dto.response;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@Builder
public class SlideResponseDto {

  private UUID id;

  private String path;

  private String name;

  private int order;

  private boolean active;
}
