package com.mor.infoservice.dto.response;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@Builder
public class PostResponseDto {

  private String title;

  private String content;

  private List<String> tag;

  private String updatedBy;
}
