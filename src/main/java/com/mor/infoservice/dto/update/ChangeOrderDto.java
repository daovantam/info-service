package com.mor.infoservice.dto.update;

import java.util.UUID;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ChangeOrderDto {

  private UUID id;
  private int index;
}
