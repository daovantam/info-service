package com.mor.infoservice.dto.update;

import java.util.List;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PostUpdateDto {

  private UUID id;

  private String title;

  private String content;

  private List<String> tag;
}
