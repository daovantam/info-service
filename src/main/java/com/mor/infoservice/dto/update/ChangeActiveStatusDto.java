package com.mor.infoservice.dto.update;

import java.util.UUID;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ChangeActiveStatusDto {

  private UUID id;
  private boolean status;
}
