package com.mor.infoservice.dto.create;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PostCreateDto {

  private String title;

  private String content;

  private List<String> tag;
}
