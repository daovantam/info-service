package com.mor.infoservice.internal.client;


import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
final class ServiceClientFallBackFactory implements FallbackFactory<ManagementServiceClient> {

  @Override
  public ManagementServiceClient create(Throwable throwable) {
    return new ManagementServiceClient() {
    };
  }
}
