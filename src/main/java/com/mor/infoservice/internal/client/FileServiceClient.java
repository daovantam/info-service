package com.mor.infoservice.internal.client;

import com.mor.infoservice.dto.UploadFileToS3Dto;
import com.mor.infoservice.factory.GeneralResponse;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "file-service/file", fallbackFactory = ServiceClientFallBackFactory.class)
@RibbonClient(name = "file-service")
public interface FileServiceClient {

  @PostMapping(value = "/api/v1/upload")
  ResponseEntity<GeneralResponse<String>> uploadFileToS3(UploadFileToS3Dto uploadFileToS3Dto);
}
