package com.mor.infoservice.internal.client;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "management-service/manage", fallbackFactory = ServiceClientFallBackFactory.class)
@RibbonClient(name = "management-service")
public interface ManagementServiceClient {

}
