package com.mor.infoservice.repository;

import com.mor.infoservice.entity.Slide;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SlideRepository extends JpaRepository<Slide, UUID>,
    JpaSpecificationExecutor<Slide> {

  @Query(value = "select coalesce(max(order2), 0) from slide", nativeQuery = true)
  Integer selectMaxOrder();

  List<Slide> getAllByIdIn(List<UUID> uuids);

  List<Slide> findAllByActive(boolean active);

  List<Slide> findAllByActiveOrderByOrder(boolean active);
}
