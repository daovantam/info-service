package com.mor.infoservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "post")
@Setter
@Getter
public class Post extends BaseEntity {

  @Column(name = "title", length = 500)
  private String title;

  @Column(name = "content", length = 2000)
  private String content;

  @Column(name = "tag")
  private String tag;
}
