package com.mor.infoservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "slide")
public class Slide extends BaseEntity {

  @Column(name = "path")
  private String path;

  @Column(name = "name")
  private String name;

  @Column(name = "order2")
  private int order;

  @Column(name = "active")
  private boolean active;
}
