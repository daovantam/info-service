package com.mor.infoservice.aop;

import com.mor.infoservice.config.QueryMonitoringConfig;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
@Slf4j
public class RepositoryAspect {

  @Autowired
  private QueryMonitoringConfig queryMonitoringConfig;

  @Around("execution(* com.mor.infoservice.repository.*.*(..))")
  public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
    long start = System.currentTimeMillis();

    Object proceed = joinPoint.proceed();

    long executionTime = System.currentTimeMillis() - start;
    String message = joinPoint.getSignature() + " executed in " + executionTime + "ms";

    if (executionTime < queryMonitoringConfig.getQueryThreshold()) {
      log.info(message);

    } else {
      log.warn("{} ---> SLOW QUERY", message);
    }

    return proceed;
  }
}