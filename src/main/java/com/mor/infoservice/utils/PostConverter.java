package com.mor.infoservice.utils;

import com.mor.infoservice.dto.create.PostCreateDto;
import com.mor.infoservice.dto.response.PostResponseDto;
import com.mor.infoservice.dto.update.PostUpdateDto;
import com.mor.infoservice.entity.Post;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class PostConverter implements
    BaseConverter<PostCreateDto, PostUpdateDto, PostResponseDto, Post> {

  @Override
  public Post createDtoToEntity(PostCreateDto postCreateDto, Principal principal) {
    Post post = new Post();
    post.setTitle(postCreateDto.getTitle());
    post.setContent(postCreateDto.getContent());
    StringBuilder tags = new StringBuilder();
    postCreateDto.getTag().forEach(tags::append);
    post.setTag(tags.toString());
    post.setTag(postCreateDto.getTitle());
    post.setCreatedBy(principal.getName());
    post.setUpdatedBy(principal.getName());
    return post;
  }

  @Override
  public boolean updateDtoToEntity(Post post, PostUpdateDto postUpdateDto, Principal principal) {
    boolean checkUpdate = false;
    StringBuilder tags = new StringBuilder();
    postUpdateDto.getTag().forEach(tags::append);
    if (!post.getTitle().equals(postUpdateDto.getTitle())) {
      checkUpdate = true;
      post.setTitle(postUpdateDto.getTitle());
    }
    if (!post.getContent().equals(postUpdateDto.getContent())) {
      checkUpdate = true;
      post.setContent(postUpdateDto.getContent());
    }
    if (!post.getTag().equals(tags.toString())) {
      checkUpdate = true;
      post.setTag(tags.toString());
    }
    if (checkUpdate) {
      post.setUpdatedBy(principal.getName());
    }
    return checkUpdate;
  }

  @Override
  public PostResponseDto entityToDtoResponse(Post post) {
    List<String> tags = new ArrayList<>();
    if (post.getTag() != null) {
      String[] tagArray = post.getTag().split(";");
      tags = Arrays.asList(tagArray);
    }
    return PostResponseDto.builder().title(post.getTitle()).content(post.getContent()).tag(tags)
        .updatedBy(
            post.getUpdatedBy()).build();
  }
}
