package com.mor.infoservice.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.TimeZone;

public class DateTimeUtils {

  public static final String FM_DATE = "yyyy-MM-dd HH:mm:ss";
  public static final String TIMEZONE_UTC = "UTC";

  public LocalDateTime converterUTCtoLocalDateTime(Timestamp timestamp) {
    if (timestamp == null) {
      return null;
    }
    LocalDateTime localDateTime = timestamp.toLocalDateTime();
    ZonedDateTime zonedDateTime = localDateTime.atZone(ZoneId.systemDefault());
    localDateTime = zonedDateTime.toLocalDateTime();
    return localDateTime;
  }

  public static Date getDateFromString(String value, String format, String timeZoneId)
      throws ParseException {
    if ((value != null) && (!("").equals(value)) && (format != null) && (!("").equals(format))) {
      SimpleDateFormat sp = new SimpleDateFormat(format);
      sp.setTimeZone(TimeZone.getTimeZone(timeZoneId));
      return sp.parse(value);
    }
    return null;
  }

  public static String getStringFromDate(Date date, String format, String timeZoneId) {

    if (date != null && format != null && !("").equals(format)) {
      SimpleDateFormat sp = new SimpleDateFormat(format);
      sp.setTimeZone(TimeZone.getTimeZone(timeZoneId));
      String result = "";
      result = sp.format(date);
      return result;
    }

    return null;
  }
}
