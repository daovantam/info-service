package com.mor.infoservice.utils;

import com.mor.infoservice.dto.response.SlideResponseDto;
import com.mor.infoservice.entity.Slide;
import org.springframework.stereotype.Component;

@Component
public class SlideConverter {

  public SlideResponseDto entityToResponseDto(Slide slide) {
    return SlideResponseDto.builder().id(slide.getId()).active(slide.isActive())
        .name(slide.getName()).order(slide.getOrder()).path(slide.getPath()).build();
  }
}
