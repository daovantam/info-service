package com.mor.infoservice.search;

import com.mor.infoservice.dto.search.SearchCriteria;
import com.mor.infoservice.entity.Post;
import com.mor.infoservice.entity.Slide;
import com.mor.infoservice.utils.DateTimeUtils;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;

@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class ObjectSpecification implements Specification {

  private SearchCriteria criteria;

  private Class T;

  @Override
  public Predicate toPredicate(Root root, CriteriaQuery query, CriteriaBuilder builder) {
    log.info("START convert ================= {}", criteria.getKey());
    Predicate pre = null;
    try {
      String key = criteria.getKey();
      String operation = criteria.getOperation().toLowerCase();
      Class clasz = getClass(key, this.T);
      Object value;
      value = convertObject(clasz, criteria.getValue());
      switch (operation) {
        case ":":
          if (String.class.equals(clasz)) {
            pre = builder.equal(builder.lower(root.get(key)), value);
          } else {
            pre = builder.equal(root.get(key), value);
          }
          break;
        case ">":
          pre = builder.greaterThan(root.get(key), (Comparable) value);
          break;
        case "<":
          pre = builder.lessThan(root.get(key), (Comparable) value);
          break;
        case ">=":
          pre = builder.greaterThanOrEqualTo(root.get(key), (Comparable) value);
          break;
        case "<=":
          pre = builder.lessThanOrEqualTo(root.get(key), (Comparable) value);
          break;
        case "like":
          if (String.class.equals(clasz)) {
            pre = builder
                .like(builder.lower(root.get(key)), "%" + value + "%");
          } else {
            pre = builder.like(root.get(key), "%" + value + "%");
          }
          break;
        case "in":
          if (String.class.equals(clasz)) {
            pre = builder.lower(root.get(key)).in(value);
          } else {
            pre = root.get(key).in(value);
          }
          break;
        default:
          break;
      }
    } catch (Exception e) {
      log.error("toPredicate : ", e);
    }
    return pre;
  }

  private Class getClass(String key, Class T) {
    Class claszParent = T.getSuperclass();
    Class claszResult = null;
    try {
      claszResult = T.getDeclaredField(key).getType();
    } catch (Exception e) {
      log.info("Cann't see this field in class");
    }
    try {
      if (claszResult == null) {
        claszResult = claszParent.getDeclaredField(key).getType();
      }
    } catch (Exception e) {
      log.info("Cann't see this field in class parent: ");
    }
    return claszResult;
  }

  private Object convertObject(Class tz, Object object) throws ParseException {
    Object value;
    if (object.getClass().equals(ArrayList.class)) {
      value = convertListByType(tz, (List<Object>) object);
    } else {
      List<Object> lstTmp = new ArrayList<>();
      lstTmp.add(object);
      List<Object> listResult = convertListByType(tz, lstTmp);
      value = listResult.get(0);
    }
    return value;
  }

  private List<Object> convertListByType(Class tz, List<Object> lstInput) throws ParseException {
    List<Object> listResult = new ArrayList<>();
    if (Timestamp.class.equals(tz)) {
      for (Object item : lstInput) {
        listResult.add(new Timestamp(DateTimeUtils
            .getDateFromString((String) item, DateTimeUtils.FM_DATE, DateTimeUtils.TIMEZONE_UTC)
            .getTime()));
      }
    } else if (UUID.class.equals(tz)) {
      for (Object item : lstInput) {
        listResult.add(UUID.fromString((String) item));
      }
    } else if (Post.class.equals(tz)) {
      for (Object item : lstInput) {
        Post entity = new Post();
        entity.setId(UUID.fromString((String) item));
        listResult.add(entity);
      }
    } else if (Slide.class.equals(tz)) {
      for (Object item : lstInput) {
        Slide entity = new Slide();
        entity.setId(UUID.fromString((String) item));
        listResult.add(entity);
      }
    } else {
      listResult = lstInput;
    }
    if (String.class.equals(tz)) {
      listResult = listResult.stream().map(o -> o.toString().toLowerCase())
          .collect(Collectors.toList());
    }
    return listResult;
  }
}

