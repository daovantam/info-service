package com.mor.infoservice.search;

import com.mor.infoservice.dto.search.SearchCriteria;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

@AllArgsConstructor
public class SpecificationBuilder {

  private Class T;

  public Specification buildCriteria(List<SearchCriteria> params) {
    if (params == null) {
      return null;
    }
    List<Specification> specs =
        params.stream().map(n -> new ObjectSpecification(n, T)).collect(Collectors.toList());
    Specification specification = null;
    for (int i = 0; i < params.size(); i++) {
      specification =
          params.get(i).isOrPredicate() ? Specification.where(specification).or(specs.get(i))
              : Specification.where(specification).and(specs.get(i));
    }
    return specification;
  }
}
