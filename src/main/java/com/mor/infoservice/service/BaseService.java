package com.mor.infoservice.service;

import com.mor.infoservice.dto.response.SearchDtoResponse;
import com.mor.infoservice.dto.search.SearchCriteriaDto;
import java.security.Principal;
import java.util.UUID;

public interface BaseService<C, U, R> {

  R create(C c, Principal principal);

  R update(U u, Principal principal);

  void delete(UUID uuid, Principal principal);

  R getById(UUID uuid);

  SearchDtoResponse<R> search(SearchCriteriaDto searchCriteriaDto);
}
