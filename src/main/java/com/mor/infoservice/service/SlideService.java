package com.mor.infoservice.service;

import com.mor.infoservice.dto.response.SlideResponseDto;
import com.mor.infoservice.dto.update.ChangeActiveStatusDto;
import com.mor.infoservice.dto.update.ChangeOrderDto;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public interface SlideService {

  SlideResponseDto create(MultipartFile multipartFile, boolean active, Principal principal)
      throws IOException;

  SlideResponseDto active(ChangeActiveStatusDto changeActiveStatusDto, Principal principal);

  List<SlideResponseDto> changeOrder(List<ChangeOrderDto> changeOrderDtos, Principal principal);

  List<SlideResponseDto> getCurrentActive();
  List<SlideResponseDto> getAllSlide();
}
