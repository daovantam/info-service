package com.mor.infoservice.service.impl;

import com.mor.infoservice.constant.ResponseStatusCodeEnum;
import com.mor.infoservice.dto.create.PostCreateDto;
import com.mor.infoservice.dto.response.PostResponseDto;
import com.mor.infoservice.dto.response.SearchDtoResponse;
import com.mor.infoservice.dto.response.SearchPreProcess;
import com.mor.infoservice.dto.search.SearchCriteriaDto;
import com.mor.infoservice.dto.update.PostUpdateDto;
import com.mor.infoservice.entity.Post;
import com.mor.infoservice.exception.custom.NoDataHasBeenChangeException;
import com.mor.infoservice.repository.PostRepository;
import com.mor.infoservice.search.SpecificationBuilder;
import com.mor.infoservice.service.PostService;
import com.mor.infoservice.service.SearchService;
import com.mor.infoservice.utils.PostConverter;
import java.security.Principal;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PostServiceImpl implements PostService {

  private final PostRepository postRepository;
  private final PostConverter postConverter;

  private final SearchService<Post> searchService;

  private final SpecificationBuilder builder = new SpecificationBuilder(Post.class);

  @Autowired
  public PostServiceImpl(PostRepository postRepository,
      PostConverter postConverter,
      SearchService<Post> searchService) {
    this.postRepository = postRepository;
    this.postConverter = postConverter;
    this.searchService = searchService;
  }

  @Override
  public PostResponseDto create(PostCreateDto postCreateDto, Principal principal) {
    Post post = postConverter.createDtoToEntity(postCreateDto, principal);
    Post created = postRepository.save(post);
    return postConverter.entityToDtoResponse(created);
  }

  @Override
  public PostResponseDto update(PostUpdateDto postUpdateDto, Principal principal) {
    Post existed = postRepository.findById(postUpdateDto.getId())
        .orElseThrow(() -> new EntityExistsException("post not exist"));
    boolean isUpdate = postConverter.updateDtoToEntity(existed, postUpdateDto, principal);
    if (isUpdate) {
      Post update = postRepository.save(existed);
      return postConverter.entityToDtoResponse(update);
    }
    throw new NoDataHasBeenChangeException(ResponseStatusCodeEnum.NO_DATA_CHANGE);
  }

  @Override
  public void delete(UUID uuid, Principal principal) {
    log.info("delete Post with ID: {} by {}", uuid, principal.getName());
    postRepository.deleteById(uuid);
  }

  @Override
  public PostResponseDto getById(UUID uuid) {
    return postConverter.entityToDtoResponse(postRepository.findById(uuid)
        .orElseThrow(() -> new EntityNotFoundException("post not exist")));
  }

  @Override
  public SearchDtoResponse<PostResponseDto> search(SearchCriteriaDto searchCriteriaDto) {
    SearchDtoResponse<PostResponseDto> searchDtoResponse = new SearchDtoResponse<>();
    SearchPreProcess<Post> provinceSearchPreProcess = searchService
        .search(searchCriteriaDto, builder);
    Page<Post> provinces = postRepository
        .findAll(provinceSearchPreProcess.getSpecification(),
            provinceSearchPreProcess.getPageable());
    List<PostResponseDto> provinceResponseDtoList = provinces.getContent().stream().parallel()
        .map(postConverter::entityToDtoResponse).collect(Collectors.toList());
    searchDtoResponse.setCurrentPage(searchCriteriaDto.getPage());
    searchDtoResponse.setPageSize(searchCriteriaDto.getPageSize());
    searchDtoResponse.setTotalPage(provinces.getTotalPages());
    searchDtoResponse.setTotalElement(provinces.getTotalElements());
    searchDtoResponse.setListObject(provinceResponseDtoList);
    return searchDtoResponse;
  }
}
