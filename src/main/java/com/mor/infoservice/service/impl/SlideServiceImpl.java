package com.mor.infoservice.service.impl;

import com.mor.infoservice.dto.UploadFileToS3Dto;
import com.mor.infoservice.dto.response.SlideResponseDto;
import com.mor.infoservice.dto.update.ChangeActiveStatusDto;
import com.mor.infoservice.dto.update.ChangeOrderDto;
import com.mor.infoservice.entity.Slide;
import com.mor.infoservice.internal.client.FileServiceClient;
import com.mor.infoservice.repository.SlideRepository;
import com.mor.infoservice.service.SlideService;
import com.mor.infoservice.utils.SlideConverter;
import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service
@Slf4j
public class SlideServiceImpl implements SlideService {

  private final SlideRepository slideRepository;

  private final SlideConverter slideConverter;

  private final FileServiceClient fileServiceClient;
  @Autowired
  public SlideServiceImpl(SlideRepository slideRepository,
      SlideConverter slideConverter,
      FileServiceClient fileServiceClient) {
    this.slideRepository = slideRepository;
    this.slideConverter = slideConverter;
    this.fileServiceClient = fileServiceClient;
  }

  @Override
  public SlideResponseDto create(MultipartFile multipartFile, boolean active, Principal principal)
      throws IOException {
    File file = new File("/images/" + multipartFile.getOriginalFilename());
    multipartFile.transferTo(file);
    String folderName = "Slide";
    UploadFileToS3Dto uploadFileToS3Dto = new UploadFileToS3Dto(folderName,
        multipartFile.getOriginalFilename(), FileUtils.readFileToByteArray(file));
    String result = Objects
        .requireNonNull(fileServiceClient.uploadFileToS3(uploadFileToS3Dto).getBody()).getData();
    if (file.delete()) {
      log.info("deleted {} in local", file.getName());
    }
    Slide slide = new Slide();
    slide.setActive(active);
    slide.setName(multipartFile.getOriginalFilename());
    slide.setPath(result);
    if (active) {
      slide.setOrder(slideRepository.selectMaxOrder() + 1);
    } else {
      slide.setOrder(0);
    }
    slide.setCreatedBy(principal.getName());
    slide.setUpdatedBy(principal.getName());
    Slide created = slideRepository.save(slide);
    return slideConverter.entityToResponseDto(created);
  }

  @Override
  public SlideResponseDto active(ChangeActiveStatusDto changeActiveStatusDto, Principal principal) {
    Slide existed = slideRepository.findById(changeActiveStatusDto.getId())
        .orElseThrow(() -> new EntityNotFoundException("slide not found"));
    if (!changeActiveStatusDto.isStatus() && existed.isActive()) {
      existed.setOrder(0);
    } else if (!existed.isActive() && changeActiveStatusDto.isStatus()) {
      existed.setOrder(slideRepository.selectMaxOrder() + 1);
    } else if (existed.isActive() == changeActiveStatusDto.isStatus()) {
      return slideConverter.entityToResponseDto(existed);
    }
    existed.setActive(changeActiveStatusDto.isStatus());
    existed.setUpdatedBy(principal.getName());
    Slide updated = slideRepository.save(existed);
    return slideConverter.entityToResponseDto(updated);
  }

  @Override
  @Transactional
  public List<SlideResponseDto> changeOrder(List<ChangeOrderDto> changeOrderDtos,
      Principal principal) {
    List<UUID> uuids = changeOrderDtos.stream().map(ChangeOrderDto::getId)
        .collect(Collectors.toList());
    List<Slide> slideList = slideRepository.findAllByActive(true);
    slideList = slideList.stream().peek(slide -> {
      slide.setActive(false);
      slide.setOrder(0);
    }).collect(Collectors.toList());
    slideRepository.saveAll(slideList);
    List<Slide> slides = slideRepository.getAllByIdIn(uuids);
    slides.forEach(slide -> {
      changeOrderDtos.stream()
          .filter(changeOrderDto -> changeOrderDto.getId().compareTo(slide.getId()) == 0)
          .forEach(changeOrderDto -> {
            slide.setOrder(changeOrderDto.getIndex());
            slide.setActive(true);
          });
      slide.setUpdatedBy(principal.getName());
    });

    List<Slide> updated = slideRepository.saveAll(slides);
    return updated.stream().map(
        slideConverter::entityToResponseDto)
        .sorted(Comparator.comparing(SlideResponseDto::getOrder)).collect(Collectors.toList());
  }

  @Override
  public List<SlideResponseDto> getCurrentActive() {
    List<Slide> slides = slideRepository.findAllByActiveOrderByOrder(true);
    return slides.stream().map(slideConverter::entityToResponseDto).collect(Collectors.toList());
  }

  @Override
  public List<SlideResponseDto> getAllSlide() {
    List<Slide> slides = slideRepository.findAll();
    return slides.stream().map(slideConverter::entityToResponseDto).collect(Collectors.toList());
  }
}
