package com.mor.infoservice.service;


import com.mor.infoservice.dto.response.SearchPreProcess;
import com.mor.infoservice.dto.search.SearchCriteriaDto;
import com.mor.infoservice.search.SpecificationBuilder;

public interface SearchService<T> {

  SearchPreProcess<T> search(SearchCriteriaDto searchCriteriaDto, SpecificationBuilder builder);
}
