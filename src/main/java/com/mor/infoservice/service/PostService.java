package com.mor.infoservice.service;

import com.mor.infoservice.dto.create.PostCreateDto;
import com.mor.infoservice.dto.response.PostResponseDto;
import com.mor.infoservice.dto.update.PostUpdateDto;
import org.springframework.stereotype.Service;

@Service
public interface PostService extends BaseService<PostCreateDto, PostUpdateDto, PostResponseDto> {

}
