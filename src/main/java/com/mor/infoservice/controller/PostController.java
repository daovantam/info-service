package com.mor.infoservice.controller;

import com.mor.infoservice.dto.create.PostCreateDto;
import com.mor.infoservice.dto.response.PostResponseDto;
import com.mor.infoservice.dto.response.SearchDtoResponse;
import com.mor.infoservice.dto.search.SearchCriteriaDto;
import com.mor.infoservice.dto.update.PostUpdateDto;
import com.mor.infoservice.factory.ResponseFactory;
import com.mor.infoservice.service.PostService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.security.Principal;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/post")
@Slf4j
public class PostController {

  private final ResponseFactory responseFactory;
  private final PostService postService;

  @Autowired
  public PostController(ResponseFactory responseFactory,
      PostService postService) {
    this.responseFactory = responseFactory;
    this.postService = postService;
  }

  @ApiOperation(value = "create Post", response = PostResponseDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Successfully create Post"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PostMapping
  public ResponseEntity<Object> createPost(@RequestBody PostCreateDto postCreateDto,
      Principal principal) {
    PostResponseDto postResponseDto = postService.create(postCreateDto, principal);
    return responseFactory.created(postResponseDto, PostResponseDto.class);
  }

  @ApiOperation(value = "update Post", response = PostResponseDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Successfully update Post"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PutMapping
  public ResponseEntity<Object> updatePost(@RequestBody PostUpdateDto postUpdateDto,
      Principal principal) {
    PostResponseDto postResponseDto = postService.update(postUpdateDto, principal);
    return responseFactory.success(postResponseDto, PostResponseDto.class);
  }

  @ApiOperation(value = "delete Post")
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Successfully delete Post"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @DeleteMapping("/{id}")
  public ResponseEntity<Object> deletePost(@PathVariable("id") UUID id,
      Principal principal) {
    postService.delete(id, principal);
    return responseFactory.success();
  }

  @ApiOperation(value = "get Post", response = PostResponseDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Successfully get Post"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @GetMapping("/{id}")
  public ResponseEntity<Object> getPost(@PathVariable("id") UUID id) {
    PostResponseDto responseDto = postService.getById(id);
    return responseFactory.success(responseDto, PostResponseDto.class);
  }

  @ApiOperation(value = "search Post", response = PostResponseDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Successfully search Post"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PostMapping("/search")
  public ResponseEntity<Object> searchPost(@RequestBody SearchCriteriaDto searchCriteriaDto) {
    SearchDtoResponse<PostResponseDto> responseDto = postService.search(searchCriteriaDto);
    return responseFactory.success(responseDto, SearchDtoResponse.class);
  }
}
