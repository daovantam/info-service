package com.mor.infoservice.controller;

import com.mor.infoservice.dto.response.SlideResponseDto;
import com.mor.infoservice.dto.update.ChangeActiveStatusDto;
import com.mor.infoservice.dto.update.ChangeOrderDto;
import com.mor.infoservice.factory.ResponseFactory;
import com.mor.infoservice.service.SlideService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api/v1/slide")
public class SlideController {

  private final ResponseFactory responseFactory;

  private final SlideService slideService;

  @Autowired
  public SlideController(ResponseFactory responseFactory,
      SlideService slideService) {
    this.responseFactory = responseFactory;
    this.slideService = slideService;
  }

  @ApiOperation(value = "create Slide", response = SlideResponseDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Successfully create Slide"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PostMapping
  public ResponseEntity<Object> createSlide(@RequestParam("file") MultipartFile file,
      @RequestParam("active") boolean active, Principal principal) throws IOException {

    SlideResponseDto slideResponseDto = slideService.create(file, active, principal);
    return responseFactory.created(slideResponseDto, SlideResponseDto.class);
  }

  @ApiOperation(value = "change status slide", response = SlideResponseDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Successfully change status slide"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PutMapping("/change-status")
  public ResponseEntity<Object> changeActiveSlide(
      @RequestBody ChangeActiveStatusDto changeActiveStatusDto, Principal principal) {

    SlideResponseDto slideResponseDto = slideService.active(changeActiveStatusDto, principal);
    return responseFactory.success(slideResponseDto, SlideResponseDto.class);
  }

  @ApiOperation(value = "change order", response = SlideResponseDto.class, responseContainer = "List")
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Successfully change order slide"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PutMapping("/change-order")
  public ResponseEntity<Object> changeOrder(@RequestBody List<ChangeOrderDto> changeOrderDtos,
      Principal principal) {

    List<SlideResponseDto> slideResponseDto = slideService.changeOrder(changeOrderDtos, principal);
    return responseFactory.success(slideResponseDto, List.class);
  }

  @ApiOperation(value = "getCurrentSlide", response = SlideResponseDto.class, responseContainer = "List")
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Successfully getCurrentSlide"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @GetMapping
  public ResponseEntity<Object> getCurrentSlide() {

    List<SlideResponseDto> slideResponseDto = slideService.getCurrentActive();
    return responseFactory.success(slideResponseDto, List.class);
  }

  @ApiOperation(value = "getAllSlide", response = SlideResponseDto.class, responseContainer = "List")
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Successfully getAllSlide"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @GetMapping("/all")
  public ResponseEntity<Object> getAllSlide() {

    List<SlideResponseDto> slideResponseDto = slideService.getAllSlide();
    return responseFactory.success(slideResponseDto, List.class);
  }
}
