FROM openjdk:8
ADD target/info-service.jar info-service.jar
EXPOSE 8084
RUN mkdir /images
ENTRYPOINT ["java", "-jar", "info-service.jar"]